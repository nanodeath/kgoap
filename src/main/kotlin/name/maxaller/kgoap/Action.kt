package name.maxaller.kgoap

/**
 * An action is any action the Agent can perform.
 *
 * It may have [preconditions] (features that must be present in the state in order to be considered) and postconditions
 * (features that are added to the world state as a result of this action being performed).
 *
 * An Action will return an [ActionResult], which is just the cost of the action plus optionally additional data.
 * If you only need to return the cost, use [CostActionResult].
 */
interface Action<A, R : ActionResult> {
    /**
     * Features that must be present in the state in order for this action to be considered.
     * Can be empty.
     */
    val preconditions: Map<Any, Any>

    /**
     * Features that will be merged into the world state if this action is performed.
     * Should *not* be empty.
     */
    val postconditions: Map<Any, Any>

    /**
     * The "actual cost" of [agent] performing this action, given [state].
     *
     * Should be positive.
     */
    fun perform(agent: A, node: Pathfinder.PathNode<*>): R
}

/** Simple implementation of [Action] that doesn't require you to return a full result object, just the cost. */
interface SimpleAction<A> : Action<A, CostActionResult> {
    override fun perform(agent: A, node: Pathfinder.PathNode<*>) = CostActionResult(this, cost(agent, node))

    /** What's the cost of performing this action? */
    fun cost(agent: A, node: Pathfinder.PathNode<*>): Int
}

/** Encapsulates the result of performing an action -- the [cost], and optionally more (for you to consume later). */
interface ActionResult {
    val actions: List<Action<*, *>>
    val cost: Int
    val additionalState: Map<Any, Any>
        get() = emptyMap()
}

data class SimpleActionResult(
    override val actions: List<Action<*, *>>,
    override val cost: Int,
    override val additionalState: Map<Any, Any> = emptyMap()
) : ActionResult

object NullActionResult : ActionResult {
    override val actions: List<Action<*, *>> = emptyList()
    override val cost: Int = 0
}

/** [ActionResult] that encapsulates only the cost of the action. */
class CostActionResult(action: Action<*, *>, override val cost: Int) : ActionResult {
    override val actions: List<Action<*, *>> = listOf(action)
}
