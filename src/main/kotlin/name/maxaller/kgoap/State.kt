package name.maxaller.kgoap

/**
 * Encapsulates all possible "world state" that [actions][Action] might be interested in, e.g. preconditions and
 * postconditions.
 */
interface State {
    /** Produce a new [State] that contains the given [key]-[value] pair. */
    fun push(key: Any, value: Any): State = StateValue(key, value, this)

    /**
     * Produce a new [State] that contains all of the pairs from the given [map].
     *
     * Note that this is more efficient than calling [push] repeatedly. If you have a map, prefer this method.
     */
    fun pushAll(map: Map<Any, Any>): State =
        when (map.size) {
            // If you're really pushing an empty map onto this state...we don't even need to do anything.
            0 -> this
            // Slightly more efficient to use defer to [push] when we can.
            1 -> map.entries.single().let { (k, v) -> push(k, v) }
            // General case: handle the entire map using [StateMap].
            else -> StateMap(map, this)
        }

    /** Looks up the value for the given [key], or null if none could be found. */
    operator fun get(key: Any): Any?

    /** Does this state contain every key-value pair that [map] does? */
    operator fun contains(map: Map<Any, Any>): Boolean = map.all { (key, value) -> get(key) == value }

    /** Produce a NEW mutable set of all keys contained in this and previous states. */
    val keys: HashSet<Any>

    /** Distill this state into the current value of all data it represents. */
    fun flatten(): Map<Any, Any> {
        val flattened = hashMapOf<Any, Any>()
        keys.forEach { k -> flattened[k] = this[k]!! }
        return flattened
    }
}

/** Capture a single element of state. */
private data class StateValue(private val key: Any, private val value: Any, private val prev: State) : State {
    override fun get(key: Any): Any? = if (key == this.key) value else prev[key]

    override val keys
        get() = prev.keys.also { it.add(key) }
}

/** Capture a bunch of state using a [map]. */
private data class StateMap(private val map: Map<Any, Any>, private val prev: State) : State {
    override fun get(key: Any): Any? = map[key] ?: prev[key]

    override val keys
        get() = prev.keys.also { it.addAll(map.keys) }
}

/**
 * The null/empty state. Contains nothing except the empty set.
 *
 * Only used as the head of state chains.
 */
private object NullState : State {
    override fun get(key: Any): Any? = null

    override val keys
        get() = hashSetOf<Any>()

    override fun flatten() = emptyMap<Any, Any>()
}

/**
 * Convert this map into an equivalent [State] object. If [previousState] is given, stack this state on top of that one.
 */
fun Map<Any, Any>.toState(previousState: State = NullState): State =
    previousState.pushAll(this)
