package name.maxaller.kgoap

import name.maxaller.kgoap.Pathfinder.DefaultConfiguration
import java.util.*

/**
 * [Pathfinder] is the main algorithm powering this module. Given an agent, list of actions that agent is able to
 * perform, and a start and desired end state, this will discover a reasonable list of actions to transition that agent
 * from the start to desired end state. Uses [DefaultConfiguration] for configuration by default.
 *
 * It's a lightly-modified A* algorithm, modified because unlike regular A*, you need to worry about extra state
 * associated with your current path -- your current "node" (Action) is insufficient to calculate the expected state at
 * that point. Also the actions available to you depends on the actions you've taken so far and expected state. Still,
 * it tracks progress in a priority queue based on actual cost so far + expected cost to destination, just like A*.
 */
class Pathfinder<A>(
    private val agent: A,
    private val availableActions: List<Action<A, *>>,
    private val config: Configuration = DefaultConfiguration
) {
    /**
     * Find a path of [actions][Action] to take [agent] from [initialState] to [goalState], or null if no path exists.
     */
    fun findPath(
        initialState: Map<Any, Any>,
        goalState: Map<Any, Any>
    ): Result? {
        // Nodes we need to visit.
        val toVisit: PriorityQueue<PathNode<*>> = buildInitialQueue(initialState, goalState)

        // Identifies whether we've found what we think is the best way to the given state.
        // Key: State.flatten. Value: Actual cost to achieve that state.
        val bestCosts = hashMapOf<Map<Any, Any>, Int>()

        // Debug information.
        var adds = 0 // Total number of nodes added to toVisit.
        var visits = 0 // Total number of nodes visited.

        // Explore all possibilities until we run out.
        while (toVisit.isNotEmpty()) {
            val current: PathNode<*> = toVisit.remove()

            visits++

            if (current.length > config.maximumLength)
                continue

            // Is the cost simply too high to even consider?
            if (current.totalEstimatedCost > config.maximumEstimatedCost) {
                // If this element is too costly, all subsequent elements in toVisit will also be too costly.
                break
            }

            // Have we reached our goal?
            if (goalState in current.worldState) {
                return current.reconstructPath(adds = adds, visits = visits)
            }

            // Try every available action
            for (actionToTry in availableActions) {
                // Skip actions that are in our current path, unless that's allowed
                if (!config.canRepeatActions && actionToTry in current) continue

                // Skip actions with unsatisfied preconditions
                if (actionToTry.preconditions !in current.worldState) continue

                // Visiting -- speculatively execute the action to get some basic info on it.
                val element = SpeculateAction(actionToTry, agent, current)

                // Optimization: if we already know the fastest way from our initial state to the proposed state, we
                // don't need to keep exploring alternatives.
                // The state is describable by flattening the world state into a map.
                val worldStateKey: Map<Any, Any> = element.newState.flatten()
                val bestCost: Int? = bestCosts[worldStateKey]
                if (bestCost == null || element.actualCostSoFar < bestCost) {
                    // We've either never seen this state before, or the path is better than others we've seen so far.
                    bestCosts[worldStateKey] = element.actualCostSoFar
                    toVisit.add(element.constructPathNode(goalState))
                    adds++
                }
            }
        }
        return null
    }

    interface Configuration {
        /**
         * Can the same path contain the same action multiple times? Setting this to true will slow down the algorithm.
         */
        val canRepeatActions: Boolean
        /** What's the most number of actions a path can contain? */
        val maximumLength: Int
        /** What's the maximum (estimated) cost a path can have to be considered? */
        val maximumEstimatedCost: Int
    }

    open class DefaultConfiguration : Configuration {
        override val canRepeatActions = false
        override val maximumLength = Integer.MAX_VALUE
        override val maximumEstimatedCost = Integer.MAX_VALUE

        companion object : DefaultConfiguration()
    }

    // We want to speculatively execute Action.perform without constructing a full PathNode. But if the state and
    // cost are adequate, we DO want to construct a node.
    private class SpeculateAction<A, R : ActionResult>(action: Action<A, R>, agent: A, private val current: PathNode<*>) {
        private val actionResult: R = action.perform(agent, current)

        val newState: State = current.worldState.pushAll(action.postconditions).pushAll(actionResult.additionalState)
        val actualCostSoFar: Int = current.actualCostSoFar + actionResult.cost

        fun constructPathNode(goalState: Map<Any, Any>) = PathNode(
            result = actionResult,
            worldState = newState,
            actualCostSoFar = actualCostSoFar,
            estimatedCostToGoal = newState.costFrom(goalState),
            previousPathNode = current
        )
    }

    /**
     * Constructs and populates a PriorityQueue that orders [PathNode]s by their total [PathNode.totalEstimatedCost].
     *
     * The provided state maps are used to evaluate the initial [availableActions] and queue them for visiting.
     */
    private fun buildInitialQueue(
        initialStateMap: Map<Any, Any>,
        goalStateMap: Map<Any, Any>
    ): PriorityQueue<PathNode<*>> {
        val initialState = initialStateMap.toState()
        val initialEstimatedDistance = initialState.costFrom(goalStateMap)

        val startNode = PathNode(NullActionResult, initialState, 0, initialEstimatedDistance, null)
        fun <A, R : ActionResult> Action<A, R>.newPathNode(agent: A): PathNode<R> =
            perform(agent, startNode).let { actionResult: R ->
                PathNode(
                    result = actionResult,
                    worldState = initialState.pushAll(postconditions).pushAll(actionResult.additionalState),
                    actualCostSoFar = actionResult.cost,
                    estimatedCostToGoal = initialEstimatedDistance,
                    previousPathNode = startNode
                )
            }

        return availableActions.asSequence()
            .filter { it.preconditions in initialState }
            .map { action -> action.newPathNode(agent) }
            .toCollection(PriorityQueue<PathNode<*>>(compareBy { it.totalEstimatedCost }))
    }

    /** Traverse this [PathNode] to produce a ordered queue of [actions][Action] to take. */
    private fun PathNode<*>.reconstructPath(adds: Int, visits: Int): Result {
        val path = ArrayList<Result.Node<*>>()
        var node: PathNode<*>? = this
        var totalCost = 0
        while (node != null) {
            path.add(node.toResult())
            totalCost += node.result.cost
            node = node.previousPathNode
        }
        path.reverse()
        return Result(totalCost, path, adds = adds, visits = visits)
    }

    /** The final result of a [findPath] attempt. */
    class Result(
        val totalCost: Int,
        val actionResults: List<Node<*>>,
        adds: Int,
        visits: Int
    ) {
        val debugInfo by lazy(LazyThreadSafetyMode.NONE) { DebugInfo(adds, visits) }

        class Node<D : ActionResult>(val result: D)

        data class DebugInfo internal constructor(
            /** Total number of times the toVisit queue was added to. */
            val adds: Int,
            /** Total number of nodes visited. */
            val visits: Int
        )
    }

    /**
     * Represents a potential sequence of [actions][Action] the [agent] might take, as well as other required
     * bookkeeping data.
     */
    data class PathNode<R : ActionResult>(
        /** Latest actions in this path. */
        val result: R,

        /** State of the world at this juncture, after taking the above action. */
        val worldState: State,

        /** Total real cost incurred by walking this path. */
        val actualCostSoFar: Int,

        /** Estimated cost between this point and the end goal. */
        private val estimatedCostToGoal: Int,

        /** Previous path this builds upon, or null if this is the first. */
        val previousPathNode: PathNode<*>?
    ) {
        /** Sum of [actualCostSoFar] and [estimatedCostToGoal]. Basically a fitness function for this path. */
        val totalEstimatedCost = actualCostSoFar + estimatedCostToGoal

        /** Total number of [PathNode]s in this chain. */
        val length: Int = (previousPathNode?.length ?: 0) + 1

        /** Does this path contain the given action? */
        operator fun contains(action: Action<*, *>): Boolean =
            action in result.actions || (previousPathNode?.contains(action) ?: false)
    }

    private fun <D : ActionResult> Pathfinder.PathNode<D>.toResult() = Result.Node(result)
}

/** Calculate the cost from the given [goal] based on how many entries in the goal are missing. */
private fun State.costFrom(goal: Map<Any, Any>): Int = goal.entries.count { (k, v) -> get(k) != v } * 3
