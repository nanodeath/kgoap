package name.maxaller.kgoap

import name.maxaller.kgoap.NullActionResult.cost
import java.util.*

class Baker

/**
 * This is a recipe my wife made up/recalled off the top of her head. Who knows if it'd actually work, but it's good
 * enough for this example.
 *
 * 1. Mix 2 cups flour, 1/2 teaspoon salt, 2 tsp baking soda, 1/2 cup cocoa powder
 * 2. Beat two eggs, add 1/2 cup melted butter, 1 cup sugar, 1 tsp vanilla.
 * 3. Mix 1 and 2, put in cake pan.
 * 4. Put in 350 degree oven for 20 minutes.
 */

data class UseMixingBowl(private val type: String) : SimpleAction<Baker> {
    override val preconditions: Map<Any, Any> = emptyMap()
    override val postconditions: Map<Any, Any> = mapOf("mixingBowl" to type)

    override fun cost(agent: Baker, node: Pathfinder.PathNode<*>) = 1
}

data class MixIngredient(private val ingredient: String, private val bowlType: String) : SimpleAction<Baker> {
    override val preconditions: Map<Any, Any> = mapOf("mixingBowl" to bowlType)
    override val postconditions: Map<Any, Any> = mapOf("${ingredient}Mixed" to true)

    override fun cost(agent: Baker, node: Pathfinder.PathNode<*>): Int = 1
}

class BulkMixIngredients(private val bowlType: String, private vararg val ingredients: String) : SimpleAction<Baker> {
    override val preconditions: Map<Any, Any> = mapOf("mixingBowl" to bowlType)
    override val postconditions: Map<Any, Any> = ingredients.map { "${it}Mixed" to true }.toMap()

    override fun cost(agent: Baker, node: Pathfinder.PathNode<*>): Int = postconditions.size

    override fun toString(): String =
        "BulkMixIngredients(bowlType='$bowlType', ingredients=${Arrays.toString(ingredients)})"
}

data class MixEverything(private val ingredients: List<String>) : SimpleAction<Baker> {
    override val preconditions: Map<Any, Any> = ingredients.map { "${it}Mixed" to true }.toMap()
    override val postconditions: Map<Any, Any> = mapOf("allMixedTogether" to true)

    override fun cost(agent: Baker, node: Pathfinder.PathNode<*>): Int = 3
}

enum class Oven {
    HEATING, HOT
}

object StartPreheatingOven : Action<Baker, ActionResult> {
    override val preconditions: Map<Any, Any> = emptyMap()
    override val postconditions: Map<Any, Any> = mapOf("oven" to Oven.HEATING)

    override fun perform(agent: Baker, node: Pathfinder.PathNode<*>): ActionResult =

        SimpleActionResult(
            actions = listOf(this@StartPreheatingOven),
            cost = 0,
            additionalState = mapOf("ovenPreheatingTime" to node.actualCostSoFar + cost)
    )

    override fun toString(): String = javaClass.simpleName
}

data class WaitAndDoNothing(private val duration: Int) : SimpleAction<Baker> {
    override val preconditions: Map<Any, Any> = emptyMap()
    override val postconditions: Map<Any, Any> = emptyMap()

    override fun cost(agent: Baker, node: Pathfinder.PathNode<*>): Int = duration
}

data class WaitUntilOvenPreheated(private val timeToPreheat: Int) : Action<Baker, ActionResult> {
    override val preconditions: Map<Any, Any> = mapOf("oven" to Oven.HEATING)
    override val postconditions: Map<Any, Any> = mapOf("oven" to Oven.HOT)

    override fun perform(agent: Baker, node: Pathfinder.PathNode<*>): ActionResult {
        val extra = waitUntilReady(node = node, readyTime = timeToPreheat, key = "ovenPreheatingTime")
        return SimpleActionResult(extra + listOf(this@WaitUntilOvenPreheated), extra.sumBy { it.cost(agent, node) })
    }
}

private fun waitUntilReady(node: Pathfinder.PathNode<*>, readyTime: Int, key: String): List<WaitAndDoNothing> {
    val startTime = node.worldState[key] as Int? ?: return listOf(WaitAndDoNothing(1000))
    val endTime = startTime + readyTime
    val currentTime = node.actualCostSoFar
    return if (currentTime >= endTime) {
        emptyList()
    } else {
        listOf(WaitAndDoNothing(endTime - currentTime))
    }
}

object PutCakeInOven : Action<Baker, ActionResult> {
    override val preconditions: Map<Any, Any> = mapOf("oven" to Oven.HOT, "allMixedTogether" to true)
    override val postconditions: Map<Any, Any> = mapOf("cakeLocation" to "oven")

    override fun perform(agent: Baker, node: Pathfinder.PathNode<*>): ActionResult {
        return SimpleActionResult(
            actions = listOf(this@PutCakeInOven),
            cost = 0,
            additionalState = mapOf("cakeInOvenTime" to node.actualCostSoFar + cost)
        )
    }

    override fun toString(): String = javaClass.simpleName
}

data class WaitUntilCakeIsDone(private val bakeTime: Int) : Action<Baker, ActionResult> {
    override val preconditions: Map<Any, Any> = mapOf("cakeLocation" to "oven")
    override val postconditions: Map<Any, Any> = mapOf("cakeBaked" to true)

    override fun perform(agent: Baker, node: Pathfinder.PathNode<*>): ActionResult {
        val extra = waitUntilReady(node = node, readyTime = bakeTime, key = "cakeInOvenTime")
        return SimpleActionResult(
            extra + listOf(this@WaitUntilCakeIsDone),
            extra.sumBy { it.cost(agent, node) }
        )
    }
}

object PutCakeOnCounter : SimpleAction<Baker> {
    override val preconditions: Map<Any, Any> = mapOf("cakeBaked" to true)
    override val postconditions: Map<Any, Any> = mapOf("cakeLocation" to "counter")

    override fun cost(agent: Baker, node: Pathfinder.PathNode<*>) = 1

    override fun toString(): String = javaClass.simpleName
}
