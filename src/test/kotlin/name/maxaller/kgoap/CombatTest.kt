package name.maxaller.kgoap

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * Fun little test case in which there's a "combatant" looking to get a good angle on a target, but has to reload and
 * get close first.
 */
class CombatTest {
    @Test
    fun sanity() {
        val agent = Combatant()

        val pathFinder = Pathfinder(
            agent = agent,
            availableActions = allCombatantActions
        )

        // initial state: empty gun, far from enemy, not firing at enemy.
        // desired state: loaded gun, close to enemy, firing at enemy.
        val path = pathFinder.findPath(
            initialState = emptyMap(),
            goalState = mapOf("nearEnemy" to true, "firingAtEnemy" to true)
        )

        path!!
        assertThat(path.totalCost).isEqualTo(7)

        assertThat(path.actionResults.flatMap { it.result.actions }).containsExactly(
            ReloadWeapon,
            FireWeapon,
            MoveToEnemy
        )
        for (node in path.actionResults) {
            when (node.result.actions.firstOrNull()) {
                is ReloadWeapon -> {
                    val manifestedAction = node.result
                    assertThat(manifestedAction.cost).isEqualTo(3)
                }
                is FireWeapon -> {
                    val manifestedAction = node.result
                    assertThat(manifestedAction.cost).isEqualTo(1)
                }
                is MoveToEnemy -> {
                    // I REALLY wanted there to be a way to get this without casting, but it seems like there's no
                    // good way
                    val manifestedAction = node.result as ActionPathResult
                    assertThat(manifestedAction.cost).isEqualTo(3)
                    assertThat(manifestedAction.path).isEqualTo(listOf(1F to 1F, 2F to 2F, 3F to 3F))
                }
            }
        }
        println(path.actionResults.flatMap { it.result.actions })
        println(path.debugInfo)
    }

    @Test
    fun `no actions`() {
        val agent = Combatant()

        val pathFinder = Pathfinder(
            agent = agent,
            availableActions = emptyList()
        )

        val path = pathFinder.findPath(
            initialState = emptyMap(),
            goalState = mapOf("nearEnemy" to true)
        )

        assertThat(path).isNull()
    }

    @Test
    fun `maximum length`() {
        val agent = Combatant()

        val pathFinder = Pathfinder(
            agent = agent,
            availableActions = allCombatantActions,
            config = object : Pathfinder.DefaultConfiguration() {
                override val maximumLength = 2
            }
        )

        val path = pathFinder.findPath(
            initialState = emptyMap(),
            goalState = mapOf("nearEnemy" to true, "firingAtEnemy" to true)
        )

        // Steps required is 3, limit is 2
        assertThat(path).isNull()
    }

    @Test
    fun `maximum cost`() {
        val agent = Combatant()

        val pathFinder = Pathfinder(
            agent = agent,
            availableActions = allCombatantActions,
            config = object : Pathfinder.DefaultConfiguration() {
                override val maximumEstimatedCost = 3
            }
        )

        val path = pathFinder.findPath(
            initialState = emptyMap(),
            goalState = mapOf("nearEnemy" to true, "firingAtEnemy" to true)
        )

        // Estimated cost is 4, limit is 3
        println(path?.totalCost)
        assertThat(path).isNull()
    }
}
