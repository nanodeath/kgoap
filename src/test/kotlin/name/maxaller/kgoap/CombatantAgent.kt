package name.maxaller.kgoap

class Combatant

object MoveToTotalCover : Action<Combatant, ActionPathResult> {
    override val preconditions: Map<Any, Any> = emptyMap()
    override val postconditions: Map<Any, Any> = mapOf("inCover" to true, "nearEnemy" to false)

    override fun perform(agent: Combatant, node: Pathfinder.PathNode<*>) = ActionPathResult(this, listOf(1F to 1F, 1F to 4F))

    override fun toString(): String = javaClass.simpleName
}

object MoveToEnemy : Action<Combatant, ActionPathResult> {
    override val preconditions: Map<Any, Any> = emptyMap()
    override val postconditions: Map<Any, Any> = mapOf("nearEnemy" to true, "inCover" to false)

    override fun perform(agent: Combatant, node: Pathfinder.PathNode<*>) = ActionPathResult(this, listOf(1F to 1F, 2F to 2F, 3F to 3F))

    override fun toString(): String = javaClass.simpleName
}

object ReloadWeapon : SimpleAction<Combatant> {
    override val preconditions: Map<Any, Any> = emptyMap()
    override val postconditions: Map<Any, Any> = mapOf("weaponLoaded" to true)

    override fun cost(agent: Combatant, node: Pathfinder.PathNode<*>): Int = if (node.worldState["inCover"] == true) 1 else 3

    override fun toString(): String = javaClass.simpleName
}

object FireWeapon : SimpleAction<Combatant> {
    override val preconditions: Map<Any, Any> = mapOf("weaponLoaded" to true)
    override val postconditions: Map<Any, Any> = mapOf("firingAtEnemy" to true)

    override fun cost(agent: Combatant, node: Pathfinder.PathNode<*>): Int = if (node.worldState["inCover"] == true) 3 else 1

    override fun toString(): String = javaClass.simpleName
}

class ActionPathResult(action: Action<*, *>, val path: List<Pair<Float, Float>>) : ActionResult {
    override val actions: List<Action<*, *>> = listOf(action)
    override val cost: Int = path.size
}

val allCombatantActions = listOf(
    MoveToEnemy,
    ReloadWeapon,
    FireWeapon,
    MoveToTotalCover
)
