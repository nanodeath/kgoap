package name.maxaller.kgoap

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * 1. Mix 2 cups flour, 1/2 teaspoon salt, 2 tsp baking soda, 1/2 cup cocoa powder
 * 2. Beat two eggs, add 1/2 cup melted butter, 1 cup sugar, 1 tsp vanilla.
 * 3. Mix 1 and 2, put in cake pan.
 * 4. Put in 350 degree oven for 20 minutes.
 */

class BakerTest {
    @Test
    fun `lets make a cake using MixIngredient`() {
        val agent = Baker()

        val pathFinder = Pathfinder(
            agent = agent,
            availableActions = listOf(
                UseMixingBowl("dry"),
                UseMixingBowl("wet"),

                MixIngredient("flour", bowlType = "dry"),
                MixIngredient("salt", bowlType = "dry"),
                MixIngredient("baking soda", bowlType = "dry"),
                MixIngredient("cocoa powder", bowlType = "dry"),

                MixIngredient("eggs", bowlType = "wet"),
                MixIngredient("butter", bowlType = "wet"),
                MixIngredient("sugar", bowlType = "wet"),
                MixIngredient("vanilla", bowlType = "wet"),

                MixEverything(
                    listOf(
                        "flour", "salt", "baking soda", "cocoa powder",
                        "eggs", "butter", "sugar", "vanilla"
                    )
                ),

                StartPreheatingOven,
                WaitUntilOvenPreheated(10),
                PutCakeInOven,
                WaitUntilCakeIsDone(20),
                PutCakeOnCounter
            )
        )

        val path = pathFinder.findPath(
            initialState = emptyMap(),
            goalState = mapOf(
                "cakeBaked" to true, "cakeLocation" to "counter"
            )
        )

        path!!
        println(path.actionResults.flatMap { it.result.actions })
        assertThat(path.totalCost).isEqualTo(34)
        println("Total time: ${path.totalCost}")
        println(path.debugInfo)
    }

    @Test
    fun `lets make a cake using BulkMixIngredients`() {
        val agent = Baker()

        val pathFinder = Pathfinder(
            agent = agent,
            availableActions = listOf(
                UseMixingBowl("dry"),
                UseMixingBowl("wet"),

                BulkMixIngredients("dry", "flour", "salt", "baking soda", "cocoa powder"),

                BulkMixIngredients("wet", "eggs", "butter", "sugar", "vanilla"),

                MixEverything(
                    listOf(
                        "flour", "salt", "baking soda", "cocoa powder",
                        "eggs", "butter", "sugar", "vanilla"
                    )
                ),

                StartPreheatingOven,
                WaitUntilOvenPreheated(10),
                PutCakeInOven,
                WaitUntilCakeIsDone(20),
                PutCakeOnCounter
            )
        )

        val path = pathFinder.findPath(
            initialState = emptyMap(),
            goalState = mapOf("cakeBaked" to true, "cakeLocation" to "counter")
        )

        path!!
        println(path.actionResults.flatMap { it.result.actions })
        assertThat(path.totalCost).isEqualTo(34)
        println("Total time: ${path.totalCost}")
        println(path.debugInfo)
    }
}
