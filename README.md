# kGOAP

kGOAP is a Kotlin-based "educational" version of GOAP (Goal-Oriented Action Planning).

## What is GOAP?

In the reductionist terms, GOAP is what you get when you apply path-finding algorithms to AI: you have a starting point
(current world state), a goal (desired world state), and a collection of nodes connecting them (actions that can alter
world state). Since this essentially describes a graph, you can apply pathfinding algorithms like A* to the problem.

For a better explanation and lots for information, check out the following resources:
* [Jeff Orkin's MIT Page](https://alumni.media.mit.edu/~jorkin/goap.html)
* [Jeff Orkin's F.E.A.R. Whitepaper](https://alumni.media.mit.edu/~jorkin/gdc2006_orkin_jeff_fear.pdf)
* [Brent Owen's GOAP Tutorial](https://gamedevelopment.tutsplus.com/tutorials/goal-oriented-action-planning-for-a-smarter-ai--cms-20793)
 
## What is kGOAP?

kGOAP is relatively clean and simple implementation of GOAP I created for fun and education, but also:

* For you to read to better understand how GOAP works, and
* For you to port to your language/runtime of choice.

Because I'm more familiar with Kotlin than C# (Unity's language), I wrote this in Kotlin with the intent to later port
it to C#.

## What *isn't* kGOAP?

kGOAP is not intended to be a fully-featured GOAP planner. There are many features I'd *like* to add, but I fear would
complicate the implementation to the point that it would no longer be a good learning resource. At the same time, this
implementation is not intended to be a "toy" implementation, not usable in real world scenarios -- if it needs something
in order to be useful, then it should be added.

# Getting Started

There are only a few classes you should know about.

## Pathfinder

This is the brains of the operation, which actually performs the pathfinding from start to finish. You give it an agent
(e.g. player, enemy, creature) and a start and end state, and it plots a sequence of actions that agent should take.

All you need to do is construct and call Pathfinder; no need to customize it.

## State

The purpose of the agent is to change the world around it -- chop trees, move resources, attack you, etc. The state that
the agent (and actions) needs to know about and/or influence will all be captured in the state objects.

One thing that's key to understand is the way preconditions and postconditions work is very simplistic -- they're often
just key-value pairs where the value is a boolean. So essentially when you're determining whether the agent is within
range of a location, you don't model this as {distanceToTarget->3.44}, you model it as {nearTarget->true}. Remember, the
MoveToTarget action doesn't necessarily need to tell the agent exactly how to get from its current position to the
target position -- at this level, the AI's only purpose is to decide whether that's they're highest priority or not.
There will likely be a completely different system that handles pathfinding and moving from point A to point B.

You don't need to construct or work with States directly, but understanding how they work will be helpful in
implementing Actions.

## Action

Actions are logical *things* your Agents can do to affect the world (state!) around them. Every Agent has a list of all
possible Actions they're capable of performing -- walking, patrolling, hunting, eating, sleeping, and so on -- anything
that has preconditions (is my gun loaded? do I have an axe? is there room in my inventory?), postconditions (my gun is
now loaded; I now have an axe; I now have lumber in my inventory), and a cost (walking 10 paces is more time-intensive
than picking up an axe).

You *will* need to implement your own Actions, which can then be shared among your agents.

Remember that this is still high-level AI -- your Actions don't need to say *how* to accomplish something, only what's
the most appropriate next action to perform. That is, your MoveToTarget action won't say how to move, only that the
agent should move.

## Examples

Check out the unit tests directory, src/test/kotlin/.
